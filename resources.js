Vue.http.options.root = 'http://192.168.10.10:8000/api';

//window.Bill = Vue.resource('bills{/id}')

// personalisando
// segundo parâmetro são valores para os parâmetros

window.Bill = Vue.resource('bills{/id}', {}, {
    total: {method: 'GET', url: 'bills/total'}
})

window.BillReceive = Vue.resource('billsReceives{/id}', {}, {
    total: {method: 'GET', url: 'billsReceives/total'}
})