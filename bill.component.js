window.billComponent = Vue.extend({
    template: `
        <nav>
            <ul >
                <li v-for="o in menus">
                    <!--<a href="javascript:void(0)" @click.prevent="showView($event, o.id)">{{ o.name }}</a>-->
                    <!--<a v-link="{path: o.url}">{{ o.name }}</a>-->
                    <a v-link="{name: o.routeName}">{{ o.name }}</a>
                </li>
            </ul>
        </nav>
        <router-view></router-view>
    `,
    data: function () {
        return {
            menus: [
                {name: "Dashboard", routeName: 'dashboard'},
                {name: "Contas a Pagar", routeName: 'bill-pay.list'},
                {name: "Contas a Receber", routeName: 'bill-receive.list'}
            ],
        };
    }
});
Vue.component('bill-component', billComponent);