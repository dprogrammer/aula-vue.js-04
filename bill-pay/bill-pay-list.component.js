window.billPayListComponent = Vue.extend({
    template: `
        <style type="text/css">
            .pago {
                color: green;
            }
            .nao-pago {
                color: red;
            }
        </style>
        <table border="1" cellpadding="10">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Vencimento</th>
                    <th>Nome</th>
                    <th>Valor</th>
                    <th>Paga?</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="(index,o) in bills"> <!-- primeiro exemplo = o in bills" -->
                    <td>{{ index + 1 }}</td>
                    <td>{{ o.date_due }}</td>
                    <td>{{ o.name }}</td>
                    <td>{{ o.value | currency 'R$ ' }}</td>
                    <td class="backg-col" :class="{'pago': o.done, 'nao-pago': !o.done}">
                        <input type="checkbox" v-model="o.done" @click="doneUpdatePay(o)" /> {{ o.done | doneLabel }}
                    </td>
                    <td>
                        <a v-link="{ name: 'bill-pay.update', params: {id: o.id} }">Editar</a> | <a href="#"  @click.prevent="deleteBill(o)">Excluir</a>
                    </td>
                </tr>
            </tbody>
        </table>

        
    `,
    //http: {
    //    root: 'http://192.168.10.10:8000/api'
    //},
    data: function () {
        return {
            //bills: this.$root.$children[0].billsPay // root é o componente Pai'
            bills: []
        };
    },
    created: function() {

        //var resource = this.$resource('bills{/id}');

        var self = this;

        Bill.query().then(function(response){ // executada quando é sucesso'
            self.bills = response.data; // corpo da requisição'
        }) ;

        // não precisa colocar barra
        //this.$http.get('bills').then(function(response){ // executada quando é sucesso'
        //    this.bills = response.data; // corpo da requisição'
        //})  
    },
    methods: {
        deleteBill: function(bill) {
            //http://stackoverflow.com/a/12582246
            var x = confirm("Deseja confirma a exclusão da conta " + bill.name + "?");
                
            if (x) {

                //var resource = this.$resource('bills{/id}');

                var self = this;

                Bill.delete({id: bill.id}).then(function(response){
                    self.bills.$remove(bill); // para remover do componente tabala

                    self.$dispatch('change-info');
                })

                /*
                this.$http.delete('bills/' + bill.id).then(function(response){
                    this.bills.$remove(bill); // para remover do componente tabala

                    this.$dispatch('change-status');
                })
                */
            }

                //this.$root.$children[0].billsPay.$remove(bill);
            
                //http://stackoverflow.com/a/35459981
        },
        doneUpdatePay: function(bill) {
            bill.done = !bill.done;
            console.log("alterar pay " + bill.name); 

            var self = this;

            Bill.update({id: bill.id}, bill).then(function(response){
                self.$dispatch('change-info');
            })   
        }
    },
    computed: {
        totalPay: function () {
            // novo status para o exercício
            var totalTemp = 0;
            var billListComponent = this.bills; //this.$root.$children[0].billsPay

            console.log("tou pay");

            for ( var i in billListComponent ) {
               totalTemp += parseFloat(billListComponent[i].value);

               console.log("valor pay: " + billListComponent[i].value);
            }

            return totalTemp;
        }
    }
});
Vue.component('bill-pay-list-component', billPayListComponent);