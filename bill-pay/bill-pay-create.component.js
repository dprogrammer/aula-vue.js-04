window.billPayCreateComponent = Vue.extend({
    template: `
        <form name="form" @submit.prevent="submit">
                <label>Vencimento:</label>
                <input type="text" v-model="bill.date_due" />
                <br><br>
                <label>Nome:</label>
                <select v-model="bill.name">
                    <!--<option v-for="o in names" value="{{ o }}">{{ o }}</option>-->
                    <!--<option v-for="o in names" v-bind:value="o">{{ o }}</option>-->
                    <option v-for="o in names" :value="o">{{ o }}</option> <!-- property binding -->
                </select>    
                <br><br>
                <label>Valor:</label>
                <input type="text" v-model="bill.value" />
                <br><br>
                <input type="submit" value="Enviar" />
            </form>
    `,
    //http: {
    //    root: 'http://192.168.10.10:8000/api'
    //},
    //props: ['bill'], não precisa mais da propriedade. usando events // no componente usa assim -> v-bind:bill="bill" -> é para passar o valor do componente estando no html'
    data: function() {
        return {
            formType: 'insert',
            names: [
                'Conta de luz',
                'Conta de água',
                'Conta de telefone',
                'Supermercado',
                'Cartão de Crédito',
                'Empréstimo',
                'Gasolina'
            ],
            bill: {
                date_due: '',
                name: '',
                value: 0,
                done: false
            }
        };
    },
    created: function(){
        console.log(this.$route.name);
        if (this.$route.name == 'bill-pay.update') {
            this.formType = 'update';

            var parametroId = this.$route.params.id; 
            this.getBill(parametroId);

            return;
        }
    },
    methods: {
        submit: function () {

            console.log(this.formType);
            
            //var resource = this.$resource('bills{/id}');

            var self = this;

            if (this.formType == 'insert') {
                // {} não está passando parâmetro'
                Bill.save({}, this.bill).then(function(response){
                    self.$dispatch('change-info');
                    self.$router.go({name: 'bill-pay.list'});
                })

                /*
                this.$http.post('bills', this.bill).then(function(response){
                    this.$dispatch('change-status');
                    this.$router.go({name: 'bill-pay.list'});
                })
                */
         
                //this.$root.$children[0].billsPay.push(this.bill);

                console.log('bill create ADD');

            } else {

                Bill.update({id: this.bill.id}, this.bill).then(function(response){
                    self.$dispatch('change-info');
                    self.$router.go({name: 'bill-pay.list'});
                })

                /*
                this.$http.put('bills/' + this.bill.id, this.bill).then(function(response){
                    this.$dispatch('change-status');
                    this.$router.go({name: 'bill-pay.list'});
                })
                */

            }

        },
        getBill: function(id) {

            // agora é global, está no arquivo resources.js'
            //var resource = this.$resource('bills{/id}');

            var self = this;

            Bill.get({id: id}).then(function(response){ // executada quando é sucesso'
                self.bill = response.data; // corpo da requisição'
            })

            // query também funciona, mas por convenção, quando é apenas um item, usa-se get'
            //resource.get({id: id}).then(function(response){ // executada quando é sucesso'
            //    this.bill = response.data; // corpo da requisição'
            //})

            /*
            this.$http.get('bills/' + id).then(function(response){ // executada quando é sucesso'
                this.bill = response.data; // corpo da requisição'
            })
            */

            //var bills = this.$root.$children[0].billsPay;
            //this.bill = bills[id];
        }
    }
});
Vue.component('bill-pay-create-component', billPayCreateComponent);