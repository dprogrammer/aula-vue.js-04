window.billPayComponent = Vue.extend({
    template: ` 
        <style type="text/css">
        .backg-col {
            background-color: #CCCCCC;
        }
        .statusVerde {
            color: green;
        }
        .statusVermelho {
            color: red;
        }
        .statusCinza {
            color: #CCCCCC;
        }
    </style>
        <h1>{{ title }}</h1>
        <h3 :class="{'statusCinza':status === false, 'statusVerde':status === 0, 'statusVermelho':status > 0}">{{ status | situacaoContasText }}</h3>

        <menu-component></menu-component>

        <router-view></router-view>

        <br>
        Total a pagar: {{ total | currency 'R$ ' }}
 
    `,
    //http: {
    //    root: 'http://192.168.10.10:8000/api'
    //},
    data: function(){ // para que o componente possa ser reaproveita, já que cada um terá o seu data
        return {
            title: "Contas a pagar",
            status: false,
            total: 0
        }; // fechando o data
    },
    created: function() {
        this.updateStatus();
        this.updateTotal();
    },
    methods: {
        calculateStatus: function(bills){
            if (!bills.length){
                this.status = false;
            }

            var count = 0;

            for (var i in bills) {
                if (!bills[i].done) {
                    count++;
                }
            }

            this.status = count;
        },
        updateStatus: function(){

            var self = this;

            Bill.query().then(function(response){ // executada quando é sucesso'
                // como agora o resource é global, this está acessando-o
                //this.calculateStatus(response.data);
                self.calculateStatus(response.data);
            });

            /*
            this.$http.get('bills').then(function(response){ 
                this.calculateStatus(response.data); 
            })
            */  
        },
        updateTotal: function(){
            var self = this;

            Bill.total().then(function(response){ // executada quando é sucesso'
                self.total = response.data.total;
            });    
        }
    },
    events: {
        'change-info': function(){
            this.updateStatus();
            this.updateTotal();
        }
    }
    /*
    computed: {
        status: function () {
            // novo status para o exercício
            var countPagos = 0;
            var countNaoPagos = 0;
            var statusTemp = false;
            var billListComponent = this.bills; //this.$root.$children[0].billsPay

            for ( var i in billListComponent ) {
               if (!billListComponent[i].done ) {
                   countNaoPagos++;
                   console.log("valor i: " + i + " - valor count: " + countNaoPagos);
               } else {
                   countPagos++;
                   console.log("valor i: " + i + " - valor count: " + countPagos);
               }
            }

            if (countPagos > 0 && countNaoPagos == 0) {
                statusTemp = 0;
            } else if (countNaoPagos > 0) {
                statusTemp = countNaoPagos;
            }

            return statusTemp;
        }
    }
    */
});

Vue.component('bill-pay-component', billPayComponent); // para registrar