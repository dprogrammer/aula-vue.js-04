window.billReceiveComponent = Vue.extend({
    template: ` 
        <style type="text/css">
        .backg-col {
            background-color: #CCCCCC;
        }
        .statusVerde {
            color: green;
        }
        .statusVermelho {
            color: red;
        }
        .statusCinza {
            color: #CCCCCC;
        }
    </style>
        <h1>{{ title }}</h1>
        <h3 :class="{'statusCinza':status === false, 'statusVerde':status === 0, 'statusVermelho':status > 0}">{{ status | situacaoContasReceiveText }}</h3>

        <menu-receive-component></menu-receive-component>

        <router-view></router-view>

        <br>
        Total a receber: {{ total | currency 'R$ ' }}
    `,
    data: function(){ // para que o componente possa ser reaproveita, já que cada um terá o seu data
        return {
            title: "Contas a receber",
            status: false,
            total: 0
        }; // fechando o data
    },
    /*computed: {
        status: function () {
            // novo status para o exercício
            var countPagos = 0;
            var countNaoPagos = 0;
            var statusTemp = false;
            var billListComponent = this.$root.$children[0].billsReceive

            for ( var i in billListComponent ) {
               if (!billListComponent[i].done ) {
                   countNaoPagos++;
                   //console.log("valor i: " + i + " - valor count: " + countNaoPagos);
               } else {
                   countPagos++;
                   //console.log("valor i: " + i + " - valor count: " + countPagos);
               }
            }

            if (countPagos > 0 && countNaoPagos == 0) {
                statusTemp = 0;
            } else if (countNaoPagos > 0) {
                statusTemp = countNaoPagos;
            }

            return statusTemp;
        }
    }*/
    created: function() {
        this.updateStatus();
        this.updateTotal();
    },
    methods: {
        calculateStatus: function(bills){
            if (!bills.length){
                this.status = false;
            }

            var count = 0;

            for (var i in bills) {
                if (!bills[i].done) {
                    count++;
                }
            }

            this.status = count;
        },
        updateStatus: function(){

            var self = this;

            BillReceive.query().then(function(response){ // executada quando é sucesso'
                // como agora o resource é global, this está acessando-o
                //this.calculateStatus(response.data);
                self.calculateStatus(response.data);
            });

            /*
            this.$http.get('bills').then(function(response){ 
                this.calculateStatus(response.data); 
            })
            */  
        },
        updateTotal: function(){
            var self = this;

            BillReceive.total().then(function(response){ // executada quando é sucesso'
                //if (response.data != null) {
                    self.total = response.data.total;
                //} else {
                //    self.total = 0;
                //}
            });    
        }
    },
    events: {
        'change-receive-info': function(){
            this.updateStatus();
            this.updateTotal();
        }
    }
});

Vue.component('bill-receive-component', billReceiveComponent); // para registrar