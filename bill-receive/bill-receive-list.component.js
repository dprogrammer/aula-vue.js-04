window.billReceiveListComponent = Vue.extend({
    template: `
        <style type="text/css">
            .recebida {
                color: green;
            }
            .nao-recebida {
                color: red;
            }
        </style>
        <table border="1" cellpadding="10">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Recebimento</th>
                    <th>Nome</th>
                    <th>Valor</th>
                    <th>Recebida?</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                <tr v-for="(index,o) in bills"> <!-- primeiro exemplo = o in bills" -->
                    <td>{{ index + 1 }}</td>
                    <td>{{ o.date_due }}</td>
                    <td>{{ o.name }}</td>
                    <td>{{ o.value | currency 'R$ ' }}</td>
                    <td class="backg-col" :class="{'recebida': o.done, 'nao-recebida': !o.done}">
                        <!--<input type="checkbox" v-model="o.done" /> {{ o.done | doneReceiveLabel }}-->
                        <input type="checkbox" v-model="o.done" @click="doneUpdateReceive(o)" /> {{ o.done | doneReceiveLabel }}
                    </td>
                    <td>
                        <a v-link="{ name: 'bill-receive.update', params: {id: o.id} }">Editar</a> | <a href="#"  @click.prevent="deleteBill(o)">Excluir</a>
                    </td>
                </tr>
            </tbody>
        </table>
    `,
    /*data: function () {
        return {
            bills: this.$root.$children[0].billsReceive // root é o componente Pai'
        };
    },
    methods: {
        deleteBill: function(bill) {
            //http://stackoverflow.com/a/12582246
            var x = confirm("Deseja confirma a exclusão da conta " + bill.name + "?");
                
            if (x)
                this.$root.$children[0].billsReceive.$remove(bill);
            
                //http://stackoverflow.com/a/35459981
        }
    },
    computed: {
        totalReceive: function () {
            // novo status para o exercício
            var totalTemp = 0;
            var billListComponent = this.$root.$children[0].billsReceive

            console.log("tou receiver");

            for ( var i in billListComponent ) {
               totalTemp += billListComponent[i].value;

               console.log("valor receiver: " + billListComponent[i].value);
            }

            return totalTemp;
        }
    }*/
    data: function () {
        return {
            bills: []
        };
    },
    created: function() {
        var self = this;

        BillReceive.query().then(function(response){ // executada quando é sucesso'
            self.bills = response.data; // corpo da requisição'
        }) ;

    },
    methods: {
        deleteBill: function(bill) {
            //http://stackoverflow.com/a/12582246
            var x = confirm("Deseja confirma a exclusão da conta " + bill.name + "?");
                
            if (x) {

                var self = this;

                BillReceive.delete({id: bill.id}).then(function(response){
                    self.bills.$remove(bill); // para remover do componente tabala

                    self.$dispatch('change-receive-info');
                })

            }

        },
        doneUpdateReceive: function(bill) {
            bill.done = !bill.done;
            console.log("alterar receive " + bill.name); 

            var self = this;

            BillReceive.update({id: bill.id}, bill).then(function(response){
                self.$dispatch('change-receive-info');
            })   
        }
    },
    computed: {
        totalPay: function () {
            var totalTemp = 0;
            var billListComponent = this.bills; //this.$root.$children[0].billsPay

            console.log("tou pay");

            for ( var i in billListComponent ) {
               totalTemp += parseFloat(billListComponent[i].value);

               console.log("valor pay: " + billListComponent[i].value);
            }

            return totalTemp;
        }
    }
});
Vue.component('bill-receive-list-component', billReceiveListComponent);